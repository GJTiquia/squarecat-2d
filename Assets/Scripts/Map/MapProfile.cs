using UnityEngine;

[CreateAssetMenu(fileName = "MapProfile", menuName = "ScriptableObjects/MapProfile", order = 0)]
public class MapProfile : ScriptableObject
{
    [SerializeField] private string _mapName;
    [SerializeField] private Map _mapPrefab;

    public string GetMapName()
    {
        return _mapName;
    }

    public Map GetMapPrefab()
    {
        return _mapPrefab;
    }
}