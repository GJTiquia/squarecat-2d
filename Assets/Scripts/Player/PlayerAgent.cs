using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAgent : MonoBehaviour
{
    public static PlayerAgent LocalPlayer;

    [Header("Movement Settings")]
    [SerializeField] private float _maxSpeed;
    [SerializeField] private float _maxAcceleration;

    [Header("References")]
    [SerializeField] private Rigidbody2D _rigidbody;

    private void Awake()
    {
        LocalPlayer = this; // There will be one and only one player in the game
    }

    private void FixedUpdate()
    {
        // Get target velocity based on player input
        Vector2 targetVelocity = PlayerInputAction.Move.normalized * _maxSpeed;

        // Find out difference between target velocity and current velocity and determine the acceleration for this frame
        Vector2 velocityDiff = targetVelocity - _rigidbody.velocity;
        Vector2 deltaAcceleration = velocityDiff / Time.fixedDeltaTime;

        // Clamp the acceleration
        if (deltaAcceleration.sqrMagnitude > _maxAcceleration * _maxAcceleration)
            deltaAcceleration = _maxAcceleration * deltaAcceleration.normalized;

        // Apply the force (F = ma)
        _rigidbody.AddForce(deltaAcceleration * _rigidbody.mass, ForceMode2D.Force);
    }
}