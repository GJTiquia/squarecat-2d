using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPauseController : MonoBehaviour
{
    public void CreatePauseWindow()
    {
        Pause.Instance.Popup();
    }

    private void Update()
    {
        if (PlayerInputAction.IsPressingPausing)
        {
            CreatePauseWindow();
        }
    }
}