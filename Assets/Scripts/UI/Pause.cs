using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    // PUBLIC
    public static Pause Instance;

    public event Action OnResume;
    public event Action OnPause;

    // PRIVATE
    [SerializeField] private Button _resumeButton, _quitButton, _returnToSocialHubButton;

    private void Awake()
    {
        Instance = this;

        string currentScene = SceneManager.GetActiveScene().name;

        if (currentScene == Const.SceneNames.SocialHubScene)
        {
            _returnToSocialHubButton.gameObject.SetActive(false);
        }

        this.gameObject.SetActive(false);
    }

    private void Start()
    {
        _resumeButton.onClick.AddListener(OnResumeClick);
        _quitButton.onClick.AddListener(OnQuitClick);
        _returnToSocialHubButton.onClick.AddListener(OnReturnToSocialHubClick);
    }

    public void Popup()
    {
        gameObject.SetActive(true);
        OnPause?.Invoke();
    }

    public void OnResumeClick()
    {
        gameObject.SetActive(false);
        OnResume?.Invoke();
    }

    public void OnQuitClick()
    {
        Application.Quit();
    }

    public void OnReturnToSocialHubClick()
    {
        SceneManager.LoadScene(Const.SceneNames.SocialHubScene);
    }
}
