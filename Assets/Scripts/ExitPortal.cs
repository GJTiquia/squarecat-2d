using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitPortal : MonoBehaviour
{
    public static void ReturnToSocialHub()
    {
        SceneManager.LoadScene(Const.SceneNames.SocialHubScene);
    }
}