using UnityEngine;

public class InteractableIndicator : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private Color _defaultColor;
    [SerializeField] private Color _activeColor;

    [Header("References")]
    [SerializeField] private SpriteRenderer _circleSprite;

    private void Awake()
    {
        _circleSprite.color = _defaultColor;
    }

    public void OnEnterInteractRange()
    {
        _circleSprite.color = _activeColor;
    }

    public void OnExitInteractRange()
    {
        _circleSprite.color = _defaultColor;
    }
}