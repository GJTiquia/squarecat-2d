using UnityEngine;

public class Collectable : MonoBehaviour
{
    public void Collect()
    {
        Map.AddScoreAndCheckWin();
        Destroy(this.gameObject);
    }
}