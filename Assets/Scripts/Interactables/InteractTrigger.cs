using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InteractTrigger : MonoBehaviour
{
    [SerializeField] private UnityEvent _onEnterInteractRange;
    [SerializeField] private UnityEvent _onExitInteractRange;
    [SerializeField] private List<Component> _interactables = new List<Component>();

    public void Trigger()
    {
        InteractWithInteractables();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        _onEnterInteractRange?.Invoke();
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        _onExitInteractRange?.Invoke();
    }

    private void InteractWithInteractables()
    {
        foreach (Component component in _interactables)
        {
            if (component is IInteractable)
            {
                IInteractable interactable = (IInteractable)component;
                interactable.Interact();
            }
            else Debug.LogError($"{component.name} is not an interactable!", component.gameObject);
        }
    }
}