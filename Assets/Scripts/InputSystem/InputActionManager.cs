using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// Used to enable all the actions OnEnable.
/// </summary>
public class InputActionManager : MonoBehaviour
{
    [SerializeField] List<InputActionAsset> m_ActionAssets;
    /// <summary>
    /// Input action assets to affect when inputs are enabled or disabled.
    /// </summary>
    public List<InputActionAsset> actionAssets
    {
        get => m_ActionAssets;
        set => m_ActionAssets = value ?? throw new ArgumentNullException(nameof(value));
    }

    /// <summary>
    /// See <see cref="MonoBehaviour"/>.
    /// </summary>
    protected void OnEnable()
    {
        EnableInput();
    }

    /// <summary>
    /// See <see cref="MonoBehaviour"/>.
    /// </summary>
    protected void OnDisable()
    {
        DisableInput();
    }

    /// <summary>
    /// Enable all actions referenced by this component.
    /// </summary>
    /// <remarks>
    /// This function will automatically be called when this <see cref="InputActionManager"/> component is enabled.
    /// However, this method can be called to enable input manually, such as after disabling it with <see cref="DisableInput"/>.
    /// <br />
    /// Note that enabling inputs will only enable the action maps contained within the referenced
    /// action map assets (see <see cref="actionAssets"/>).
    /// </remarks>
    /// <seealso cref="DisableInput"/>
    public void EnableInput()
    {
        if (m_ActionAssets == null)
            return;

        foreach (var actionAsset in m_ActionAssets)
        {
            if (actionAsset != null)
            {
                actionAsset.Enable();
            }
        }
    }

    /// <summary>
    /// Disable all actions referenced by this component.
    /// </summary>
    /// <remarks>
    /// This function will automatically be called when this <see cref="InputActionManager"/> component is disabled.
    /// However, this method can be called to disable input manually, such as after enabling it with <see cref="EnableInput"/>.
    /// <br />
    /// Note that disabling inputs will only disable the action maps contained within the referenced
    /// action map assets (see <see cref="actionAssets"/>).
    /// </remarks>
    /// <seealso cref="EnableInput"/>
    public void DisableInput()
    {
        if (m_ActionAssets == null)
            return;

        foreach (var actionAsset in m_ActionAssets)
        {
            if (actionAsset != null)
            {
                actionAsset.Disable();
            }
        }
    }
}
