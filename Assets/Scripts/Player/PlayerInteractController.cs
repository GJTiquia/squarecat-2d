using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractController : MonoBehaviour
{
    [Header("Interact Settings")]
    [SerializeField] private ContactFilter2D _interactContactFilter;

    [Header("References")]
    [SerializeField] private Collider2D _interactCollider;

    public void TryInteract()
    {
        List<Collider2D> collisionResults = new List<Collider2D>();
        int numberOfCollisions = _interactCollider.OverlapCollider(_interactContactFilter, collisionResults);
        if (numberOfCollisions > 0)
        {
            // Very basic and will interact with everything in-range, will not sort according to distance
            foreach (Collider2D collider in collisionResults)
            {
                bool hasInteractTrigger = collider.TryGetComponent(out InteractTrigger interactTrigger);
                if (!hasInteractTrigger) continue;

                interactTrigger.Trigger();
            }
        }
    }

    private void Update()
    {
        if (PlayerInputAction.IsInteracting)
            TryInteract();
    }
}