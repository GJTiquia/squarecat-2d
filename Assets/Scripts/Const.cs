public static class Const
{
    public static class SceneNames
    {
        public const string GameInitScene = "GameInitScene";
        public const string UIOnlyScene = "UIOnlyScene";
        public const string SocialHubScene = "SocialHubScene";
        public const string GameScene = "GameScene";
    }
}