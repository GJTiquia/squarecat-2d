using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputAction : MonoBehaviour
{
    public static Vector2 Move { get; private set; }
    public static bool IsInteracting { get; private set; }
    public static bool IsListeningInput { get; private set; }
    public static bool IsPressingPausing { get; private set; }

    [SerializeField] InputActionReference _moveAction;
    [SerializeField] InputActionReference _interactAction;
    [SerializeField] InputActionReference _pauseAction;

    private void Start()
    {
        Pause.Instance.OnResume += () => AddInputListener();
        Pause.Instance.OnPause += () => RemoveInputListener();
    }

    private void OnEnable()
    {
        AddInputListener();
    }

    private void OnDisable()
    {
        RemoveInputListener();
    }

    private void LateUpdate()
    {
        if (IsInteracting) IsInteracting = false; // Sets back to false after the Update loop
        if (IsPressingPausing) IsPressingPausing = false;
    }

    private void OnMoveHandler(InputAction.CallbackContext ctx)
    {
        Move = ctx.ReadValue<Vector2>();
    }

    private void OnInteractHandler(InputAction.CallbackContext ctx)
    {
        IsInteracting = true;
    }

    private void OnPauseHandler(InputAction.CallbackContext ctx)
    {
        RemoveInputListener();
        IsPressingPausing = true;
    }

    public void AddInputListener()
    {
        _moveAction.action.performed += OnMoveHandler;
        _interactAction.action.started += OnInteractHandler;
        _pauseAction.action.started += OnPauseHandler;
        IsListeningInput = true;
    }

    public void RemoveInputListener()
    {
        _moveAction.action.performed -= OnMoveHandler;
        _interactAction.action.started -= OnInteractHandler;
        _pauseAction.action.started -= OnPauseHandler;
        IsListeningInput = false;
    }
}