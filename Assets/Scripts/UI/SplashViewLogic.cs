using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashViewLogic : MonoBehaviour
{
    public static void StartButtonOnClick()
    {
        SceneManager.LoadScene(Const.SceneNames.SocialHubScene);
    }

    public static void QuitButtonOnClick()
    {
        Application.Quit();
    }
}
