using UnityEngine;
using UnityEngine.SceneManagement;

public class MapTrigger : MonoBehaviour, IInteractable
{
    [SerializeField] private MapProfile _mapProfile;

    public void Interact()
    {
        Session.MapProfile = _mapProfile;
        SceneManager.LoadScene(Const.SceneNames.GameScene);
    }
}