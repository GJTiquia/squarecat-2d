using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private PlayerAgent _playerPrefab;

    private void Start()
    {
        Map mapPrefab = Session.MapProfile.GetMapPrefab();
        Map mapInstance = Instantiate(mapPrefab);

        Vector3 spawnPosition = mapInstance.GetPlayerSpawnPoint();
        Instantiate(_playerPrefab, spawnPosition, Quaternion.identity);
    }
}