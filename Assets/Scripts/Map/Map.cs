using UnityEngine;

public class Map : MonoBehaviour
{
    private static int _totalScore;
    private static Map _instance;

    [Header("Settings")]
    [SerializeField] private int _targetScore;

    [Header("References")]
    [SerializeField] private Transform _playerSpawnPoint;
    [SerializeField] private ExitPortal _exitPortal;

    private void Awake()
    {
        _instance = this;
        _totalScore = 0;

        _exitPortal.gameObject.SetActive(false);
    }

    public static void AddScoreAndCheckWin()
    {
        _totalScore++;
        _instance.CheckWin();
    }

    public Vector3 GetPlayerSpawnPoint()
    {
        return _playerSpawnPoint.position;
    }

    private void CheckWin()
    {
        if (_totalScore >= _targetScore)
            EnableExitPotal();
    }

    private void EnableExitPotal()
    {
        _exitPortal.gameObject.SetActive(true);
    }
}