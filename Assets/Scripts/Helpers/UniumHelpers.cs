using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UniumHelpers : MonoBehaviour
{
    private static UniumHelpers _instance;

    private void Awake()
    {
        _instance = this;
    }

    public static void StartGame()
    {
        SplashViewLogic.StartButtonOnClick();
    }

    public static void TeleportLocalPlayer(Vector3 position)
    {
        PlayerAgent.LocalPlayer.transform.position = position;
    }

    public static Vector3 GetLocalPlayerPosition()
    {
        return PlayerAgent.LocalPlayer.transform.position;
    }

    public static void Interact()
    {
        PlayerAgent.LocalPlayer.GetComponentInChildren<PlayerInteractController>().TryInteract();
    }

    public static string GetCurrentMap()
    {
        return Session.MapProfile.GetMapName();
    }

    public static void PauseMenu()
    {
        PlayerAgent.LocalPlayer.GetComponentInChildren<PlayerPauseController>().CreatePauseWindow();
    }

    public static void Resume()
    {
        Pause.Instance.OnResumeClick();
    }

    public static void ReturnToSocialHub()
    {
        Pause.Instance.OnReturnToSocialHubClick();
    }

    public static void ReturnToUIOnlyScene()
    {
        SceneManager.LoadScene(Const.SceneNames.UIOnlyScene);
    }

    // HELPERS TO TEST FUNCTIONALITY
    public event Action<object> TestVoidEvent; // Remember to add the event keyword and must be Action<object>!!
    public event Action<object> TestIntEvent; // Remember to add the event keyword and must be Action<object>!!
    public static void InvokeDebugLog() => Debug.Log("UniumHelpers.InvokeDebugLog");
    public static void InvokeTestVoidEvent() => _instance.TestVoidEvent?.Invoke(null);
    public static void InvokeTestIntEvent() => _instance.TestIntEvent?.Invoke(1);
}